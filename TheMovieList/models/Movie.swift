//
//  Movie.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/3/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import Foundation

struct Movie: Codable {
    let title: String
    let overview: String
    let releaseDate: String
    let posterPath: String
    let backdropPath: String
    let voteAverage: Double
    let video: Bool
    
    
    private enum CodingKeys: String, CodingKey {
        case title = "title"
        case overview = "overview"
        case releaseDate = "release_date"
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case voteAverage = "vote_average"
        case video = "video"
    }
}


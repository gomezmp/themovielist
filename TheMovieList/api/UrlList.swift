//
//  UrlList.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 10/26/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import Foundation

/**
 Enum of all endpoints, this enum will match all the required endpoints in the Endpoints.plist file.
 */
enum UrlList {
    // MARK: - Endpoints - If you want to add a new one, this is the place.
    case popular
    case upcoming
    case top_rated
    case baseURLImage
    case baseURLImageBig
    
    /**
     This variable finds the required case in the Endpoints.plist file.
     */
    var url: String? {
        guard let dictionaryOfEndpoints = UrlList.endpointsDictionary else {
            return nil
        }
        
        switch self {
        default:
            return dictionaryOfEndpoints[String(describing: self)]
        }
    }
    
    /**
     This static variable to get the Endpoints.plist file as a Dictionary of strings.
     */
    static var endpointsDictionary: [String: String]? {
        guard let path = Bundle.main.path(forResource: "UrlsList",
                                          ofType: "plist") else {
            return nil
        }
        
        return NSDictionary(contentsOfFile: path) as? [String: String]
    }
}

//
//  ApiManager.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/3/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import Foundation

class ApiManager {
    
    let decoder = JSONDecoder()
    //let urlString = "https://api.themoviedb.org/3/movie/upcoming?api_key=4d10c2630b2894e7d6e32eb238579247"
    
    
    func getService<T: Codable>(urlString: String?, succesResp: ((_ response: T?, _ error: Error?) -> Void)? ){
        
        guard let url = URL(string: urlString ?? "") else {
            succesResp?(nil,nil)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: urlRequest) { [weak self] (data: Data?, urlResponse:URLResponse?, error:Error?) in
            
            guard
                let data = data,
                let statusCode = (urlResponse as? HTTPURLResponse)?.statusCode,
                statusCode == 200,
                error == nil else {
                    succesResp?(nil,error)
                    return
            }
            
            do {
                let myUser: T? = try self?.decoder.decode(T.self, from: data)
                succesResp?(myUser,error)
                
            } catch let error {
                print(error.localizedDescription)
                succesResp?(nil,error)
            }
            }.resume()
    }
}

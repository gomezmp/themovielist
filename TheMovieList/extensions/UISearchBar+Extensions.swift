//
//  UISearchBar+Extensions.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/3/19.
//  Copyright © 2019 Mapis. All rights reserved.
//
import UIKit

extension UISearchBar {
    
    func showKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.tintColor = UIColor.white
        keyboardToolbar.backgroundColor = UIColor.black
        keyboardToolbar.barTintColor = UIColor.black
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(self.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.inputAccessoryView = keyboardToolbar
        self.keyboardAppearance = .dark
    }
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
}

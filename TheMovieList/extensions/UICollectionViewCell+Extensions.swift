//
//  collectionViewCell+Extensions.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/3/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import Foundation

import UIKit

extension UICollectionViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}

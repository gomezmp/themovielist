//
//  SuperViewTabs.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 10/26/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import UIKit

protocol selectItem {
    func selectedItem(item: Int)
}

class SuperViewTabs: UIView {

    // MARK: - UI References
       @IBOutlet weak var imageView: UIImageView!
       @IBOutlet weak var selectItem: UIButton!
       @IBOutlet weak var label: UILabel!
       @IBOutlet var baseView: UIView!
    
    // MARK: - IBInspectables
       @IBInspectable var icon: String = "popular"
       @IBInspectable var title: String = "Popular"
       @IBInspectable var iconSelct: String = "popularSelect"
       @IBInspectable var select: Bool = false
       @IBInspectable var id: Int = 0
    
    @IBAction func itemSelectAction(_ sender: Any) {
        delegate?.selectedItem(item: id)
        changeSelect()
    }
    
    var delegate: selectItem?
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        setupUI()
    }

    // MARK: - Private Methods

    /**
     Method to setup the custom view with the nib file.
     */
    private func setupUI() {
        let bundle = Bundle(for: SuperViewTabs.self)
        
        bundle.loadNibNamed("SuperViewTabs", owner: self, options: nil)
        addSubview(baseView)
        
        baseView.frame = bounds
        baseView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        label.text = title
         if select{
               changeSelect()
           }else{
               changeUnSelect()
           }
        
    }
    
    func changeSelect(){
        select = true
        imageView.image = UIImage(named: iconSelct)
        label.textColor = UIColor(red: 115/256, green: 201/256, blue: 255/256, alpha: 1.0)
    }
    
    
   func changeUnSelect(){
       select = false
       imageView.image = UIImage(named: icon)
       label.textColor = UIColor(red: 0/256, green: 0/256, blue: 0/256, alpha: 1.0)
   }


}

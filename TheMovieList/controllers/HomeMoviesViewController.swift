//
//  ViewController.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/2/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeMoviesViewController: UIViewController {

    // MARK: - UI References
    
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    @IBOutlet weak var movieSearchBar: UISearchBar!
    
    @IBOutlet weak var popularTab: SuperViewTabs!
    @IBOutlet weak var topRatedTab: SuperViewTabs!
    @IBOutlet weak var upcomingTab: SuperViewTabs!
    
    // MARK: - Properties
    
    private var viewModel = HomeMoviesViewModel()
    private let disposeBag = DisposeBag()
    
    // MARK: - life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Private methods
    
    func setupUI(){
        configViewModel()
        configSearchBar()
        popularTab.delegate = self
        upcomingTab.delegate = self
        topRatedTab.delegate = self
    }
    
    func configViewModel(){
        
        moviesCollectionView.delegate = self
        moviesCollectionView.register(UINib(nibName: MovieCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: MovieCollectionViewCell.identifier)
        viewModel.getMoviesList(movieType: UrlList.popular)
        viewModel.delegate = self
    }
    
    private func configSearchBar(){
        movieSearchBar.delegate = self
        bindSearchBar()
        bindCollectionView()
        movieSearchBar.showKeyboard()
    }
    
    func setView(){
        DispatchQueue.main.async { 
            self.moviesCollectionView.reloadData()
        }
    }
    
}

extension HomeMoviesViewController: selectItem{
    func selectedItem(item: Int) {
        if popularTab.id == item{
            upcomingTab.changeUnSelect()
            topRatedTab.changeUnSelect()
            viewModel.getMoviesList(movieType: UrlList.popular)
            return
        }
        
        if upcomingTab.id == item{
            topRatedTab.changeUnSelect()
            popularTab.changeUnSelect()
            viewModel.getMoviesList(movieType: UrlList.upcoming)
            return
        }
        
        if topRatedTab.id == item{
            upcomingTab.changeUnSelect()
            popularTab.changeUnSelect()
            viewModel.getMoviesList(movieType: UrlList.top_rated)
            return
        }
    }
}

extension HomeMoviesViewController: HomeMoviesDelegate{
    func requestLoadedWith(load: Bool, message: String?) {
        if !load {
            setView()
        }
    }
    
}

extension HomeMoviesViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

extension HomeMoviesViewController: UICollectionViewDelegate {

    private func bindSearchBar(){
        self.movieSearchBar.rx.text.orEmpty.throttle(.milliseconds(300), scheduler: MainScheduler.instance).distinctUntilChanged()
            .flatMapLatest { query -> Observable<[Movie]> in
                return self.viewModel.filter(query)
                    .catchErrorJustReturn([])
            }.observeOn(MainScheduler.instance).bind(to: self.viewModel.movies).disposed(by: disposeBag)
    }
    
    private func bindCollectionView() {
        self.viewModel.movies.bind(to: moviesCollectionView.rx.items(cellIdentifier: MovieCollectionViewCell.identifier)) {
            (index, repository: Movie, cell) in
            (cell as! MovieCollectionViewCell).setImage(imagePath: self.viewModel.getImagePathMoview(imageUrl: repository.posterPath))
            }
            .disposed(by: disposeBag)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = MovieDetailsViewController()
        viewController.data = viewModel.movies.value[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension HomeMoviesViewController:  UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width / 3)-5, height: (collectionView.bounds.height / 3)-5)
    }
}


//
//  MovieDetailsViewController.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/4/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import UIKit
import SDWebImage

class MovieDetailsViewController: UIViewController {
    
    // MARK: - UI References
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var voteAvarageLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    var data: Movie?
    
     // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }

    // MARK: - Private methods
    
    private func setView(){
        self.navigationController?.isNavigationBarHidden = false
        
        guard let information = data else{
            navigationController?.popViewController(animated: true)
            return
        }
        
        backdropImageView.sd_setShowActivityIndicatorView(true)
        dateLabel.text = information.releaseDate
        voteAvarageLabel.text = String(information.voteAverage)
        summaryLabel.text = information.overview
        titleLabel.text = information.title
        
        if let urlbase = UrlList.baseURLImageBig.url{
            let imagePath = URL(string:"\(urlbase)\(information.backdropPath)")
            backdropImageView.sd_setImage(with: imagePath, completed: { [weak self] (_, error, _, _) in
                if error != nil{
                    self?.backdropImageView.image = UIImage(named: "iconMovie")
                }
            })
        }
        
        
    }

}

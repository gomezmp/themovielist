//
//  HomeMoviesViewModel.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/3/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa

protocol HomeMoviesDelegate: class {
    func requestLoadedWith(load: Bool, message: String?)
}

class HomeMoviesViewModel {
    
    // MARK: - Properties
    private var moviesList: PopularMovies?
    weak var delegate: HomeMoviesDelegate?
    private var apiManager: ApiManager
    var movies = BehaviorRelay<[Movie]>(value: [])
    
    init (){
        apiManager = ApiManager()
    }
    
    // MARK: - Private methods
   
    /*
        returns the path of the image
    */
    func getImagePathMoview(imageUrl: String) -> String {
        if let urlbase = UrlList.baseURLImage.url{
            return "\(urlbase)\(imageUrl)"
        }
        return ""
    }
    
    /*
        Consult the service to bring the list of movies
    */
    func getMoviesList(movieType: UrlList){
        
        guard NetworkReachabilityManager()?.isReachable == true else {
            delegate?.requestLoadedWith(load: true, message: "Sin conexión a internet")
            return
        }
        
        apiManager.getService(urlString: movieType.url ,succesResp: {
         (response: PopularMovies?, error: Error?) in
         
         guard error == nil , response != nil else {
         print(error?.localizedDescription ?? "error")
         return
         }
            self.moviesList = response
            self.movies.accept(self.moviesList?.results ?? [Movie]())
            self.delegate?.requestLoadedWith(load: false, message: nil)
         })
    }
    
    /*
        make the filter of the movies
     */
    func filter (_ query:String) -> Observable<[Movie]> {
        return Observable<[Movie]>.create { (observer) -> Disposable in
            let filtered = query.isEmpty ? self.moviesList?.results : self.moviesList?.results.filter {(($0.title ).lowercased().contains(query.lowercased()))
            }
            observer.onNext(filtered ?? [Movie]())
            observer.onCompleted()
            return Disposables.create {}
        }
    }
    
}

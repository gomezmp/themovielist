//
//  MovieCollectionViewCell.swift
//  TheMovieList
//
//  Created by Maria Paula Gómez on 8/3/19.
//  Copyright © 2019 Mapis. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var dontUseImageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        movieImageView.image = nil
    }
    
    
    func setImage(imagePath: String){
        
        movieImageView.sd_setShowActivityIndicatorView(true)
        movieImageView?.sd_setImage(with: URL(string: imagePath), completed: { [weak self] (_, error, _, _) in
            self?.dontUseImageLabel.isHidden = error == nil
        })
    }
    

}
